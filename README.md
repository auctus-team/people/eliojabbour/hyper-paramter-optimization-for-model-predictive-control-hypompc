# Hyper Paramter optimization for Model-Predictive Control-HYPOMPC


## Description

HYPO-MPC is a project focused on optimizing the hyperparameters of a Model Predictive Control (MPC) system using the NSGA-II algorithm. 
The goal is to find optimal values for MPC parameters, such as time horizon, velocity coefficient, and gamma, to enhance the performance of the control system.

The optimization process is facilitated by the NSGA-II algorithm, a multi-objective genetic algorithm that explores the parameter space to discover a set of solutions representing the Pareto front.

## Target Audience
HYPO-MPC is made for a broad audience, including researchers, engineers, and people who work with robots. 
It's here to solve the common problem of tuning MPC settings, and it does this automatically, saving time compared to doing it by hand.

## MPC Application
Originally developed for control process engineering, MPC faced limitations in real-time application due to high computation costs. However, recent technological advancements have overcome these barriers, enabling MPC's integration into real-time robotics.

## Optimization Challenges and Goals
Tuning an MPC controller presents significant challenges, including the impracticality of manually adjusting parameters for complex systems, the necessity 
for adaptive strategies in dynamic real-world scenarios, and the potential conflicts arising from parameter changes.
In HYPO-MPC, our objective is not only to overcome these challenges but also to enhance the overall performance of MPC systems.This is achieved through the optimization of key parameters, such as 
the prediction horizon (τh), state velocity weighting factor (Kv), and control input weight (γ).These optimizations aim to improve objective values related to rise time, error, and overshoot without imposing excessive computational costs.

## Methodology: Multi-Objective Optimization

HYPO-MPC employs a multi-objective optimization technique known as NSGA2, utilizing the Pymoo library. This approach focuses on tuning three key MPC parameters simultaneously, allowing for the consideration of multiple performance criteria.
The outcome of this optimization process is a Pareto front, visually representing optimal compromises between conflicting objectives. 
This method facilitates a thorough exploration of the parameter space, streamlining the tuning process while providing valuable insights into inherent trade-offs within the system.
By optimizing MPC parameters based on objective values, the designer gains flexibility in choosing parameters aligned with their preferences. This choice is informed by the trade-off plot presented through the Pareto front's optimal values.

## Current Version Application
In its current iteration, HYPO-MPC focuses on training parameters(τh, Kv and γ) for a system aiming to minimize deviation from a reference trajectory. 
The primary objective is to attain optimal performance, emphasizing fast rise time, accuracy, absence of overshoot, and computational efficiency, particularly in terms of velocity and position. Simultaneously, the system seeks to minimize control input acceleration. This version is specifically designed for training scenarios using step responses.
## Customization
Flexibility is a cornerstone of HYPO-MPC's design. Users have the freedom to customize parameters for optimization, tailoring the tool to specific needs.
Furthermore, designers retain control over critical aspects of the optimization process, including the number of populations, generations, and the choice
of Genetic Algorithm methods for selection, mutation, and crossover.

## Results and Interpretation
Upon completion of the optimization process, the results will be stored in a file, which can be further analyzed using a post-results analysis script. 
This script empowers designers to visually inspect crucial metrics, offering a detailed understanding of the optimized outcomes.
Additionally, it provides the flexibility for designers to select preferred objective values, corresponding to a combination of decision variable parameters found on the Pareto front.


# Jupyter notebook :
For the current instance, there is 2 notebooks that teach you how to run the code and analyze the data :
- [Chapter 1](https://gitlab.inria.fr/auctus-team/people/eliojabbour/hyper-paramter-optimization-for-model-predictive-control-hypompc/-/blob/master/Chapter1.ipynb?ref_type=heads) MPC trajectory in a simulation in response to step function. The study is still in 1D


-[Chapter 2](https://gitlab.inria.fr/auctus-team/people/eliojabbour/hyper-paramter-optimization-for-model-predictive-control-hypompc/-/blob/master/Chapter2.ipynb?ref_type=heads) is for optimizing the hyperparameter and finding the pareto front of the objective space parameters for the MPC. The study is still not fully customizable.


## Contributors
- [Elio Jabbour](https://gitlab.inria.fr/auctus-team/people/eliojabbour)

## Project Status
Currently, the project is still under construction.

