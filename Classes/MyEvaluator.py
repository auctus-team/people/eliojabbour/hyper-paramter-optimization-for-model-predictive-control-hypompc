# custom_classes.py
import numpy as np

from pymoo.optimize import minimize
from numpy.linalg import LinAlgError
from pymoo.core.evaluator import Evaluator
from pymoo.core.individual import Individual
from pymoo.core.population import Population
from pymoo.problems import get_problem
from pymoo.core.problem import Problem
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting

from pymoo.util.normalization import denormalize, get_extreme_points_c, get_nadir_point
import numpy as np
from pymoo.util.misc import random_permuations
from pymoo.core.individual import Individual
from pymoo.core.population import Population
from pymoo.optimize import minimize
from numpy.linalg import LinAlgError
from pymoo.core.evaluator import Evaluator
from pymoo.problems import get_problem
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting

from pymoo.util.misc import random_permuations
import math
import numpy as np

from pymoo.util.dominator import Dominator



class MyEvaluator(Evaluator):
    def __init__(self, skip_already_evaluated: bool = True, evaluate_values_of: list = ["F", "G", "H"], callback=None):

        """
        The evaluator has the purpose to glue the problem with the population/individual objects.
        Additionally, it serves as a bookkeeper to store determine the number of function evaluations of runs, time,
        and others.


        Parameters
        ----------
        skip_already_evaluated : bool
            If individual that are already evaluated shall be skipped.

        evaluate_values_of : list
            The type of values to be asked the problem to evaluated. By default all objective, ieq. and eq. constraints.

        """

        super().__init__(skip_already_evaluated, evaluate_values_of, callback)
        self.evaluate_values_of = evaluate_values_of
        self.skip_already_evaluated = skip_already_evaluated
        self.callback = callback
        self.ideal_point=[100,100,100] #initialize to a large value given that ideal point will be smaller than this
        self.nadir_point=[0,0,0] #initialize to a small value  given that nadir point will be larger than this

        # current number of function evaluations - initialized to zero
        self.n_eval = 0

    def eval(self,
             problem: Problem,
             pop: Population,
             skip_already_evaluated: bool = None,
             evaluate_values_of: list = None,
             count_evals: bool = True,
             **kwargs):

        # load the default settings from the evaluator object if not already provided
        evaluate_values_of = self.evaluate_values_of if evaluate_values_of is None else evaluate_values_of
        skip_already_evaluated = self.skip_already_evaluated if skip_already_evaluated is None else skip_already_evaluated

        # check the type of the input
        is_individual = isinstance(pop, Individual)

        # make sure the object is a population
        if is_individual:
            pop = Population().create(pop)

        # filter the index to have individual where not all attributes have been evaluated

        I = np.arange(len(pop))

        # evaluate the solutions (if there are any)
        if len(I) > 0:

            # do the actual evaluation - call the sub-function to set the corresponding values to the population
            self._eval(problem, pop[I], evaluate_values_of, **kwargs)

        # update the function evaluation counter
        if count_evals:
            self.n_eval += len(I)

        # allow to have a callback registered
        if self.callback:
            self.callback(pop)

        if is_individual:
            return pop[0]
        else:
            return pop

    def _eval(self, problem, pop, evaluate_values_of, **kwargs):

        # get the design space value from the individuals
        X = pop.get("X")

        # call the problem to evaluate the solutions
        out = problem.evaluate(X, return_values_of=evaluate_values_of, return_as_dictionary=True, skip_already_evaluated=False, **kwargs)

        # Extract the "F" values from the dictionary
        F_values = out.get("F", [])
        # Find the indices of individuals with the highest rank
        _, rank = NonDominatedSorting().do(F_values, return_rank=True)
        # print('pareto_front',rank)
        # Find indices of individuals with rank 0
        pareto_front_indices = np.where(rank == 0)[0]

        # If there are individuals with rank 0, find the maximum rank
        if len(pareto_front_indices) > 0:
            max_rank_with_0 = np.max(F_values[pareto_front_indices],axis=0)
            # print("Maximum Rank with Value 0:", max_rank_with_0)
        else:
            max_rank_with_0= np.max(F_values,axis=0)
            print("No individuals with rank 0 found.")

        # Check if there are "F" values and calculate the minimum

        min_F = np.min(F_values, axis=0)

        # Update the ideal point using the minimum "F" values
        self.ideal_point = np.minimum(self.ideal_point, min_F)
        self.nadir_point = max_rank_with_0
        print('ideal_point',self.ideal_point)
        print('nadir_point',self.nadir_point)
        # Calculate normalization
        ideal_point_matrix = np.tile(self.ideal_point, (len(F_values), 1))
        nadir_point_matrix = np.tile(self.nadir_point, (len(F_values), 1))
        epsilon_matrix = np.tile(1e-12, (len(F_values), 1))
        Normalized = (F_values - ideal_point_matrix) / (nadir_point_matrix - ideal_point_matrix + epsilon_matrix)


        out["F"] = Normalized

        # for each of the attributes set it to the problem
        for key, val in out.items():
            if val is not None:
                pop.set(key, val)

        # finally set all the attributes to be evaluated for all individuals
        pop.apply(lambda ind: ind.evaluated.update(out.keys()))



