# custom_classes.py
import numpy as np
from pymoo.operators.selection.tournament import TournamentSelection,compare
from pymoo.optimize import minimize
from pymoo.core.callback import Callback
import numpy as np
from pymoo.algorithms.moo.rnsga2 import NSGA2
from pymoo.problems import get_problem
# from Classes.NormNSGA2 import MPCOptimizationProblem



class MyCallback(Callback):
    def __init__(self) -> None:
        super().__init__()
        self.generation_count = 0
        self.PrevPop = []
        self.opt = None

    def notify(self, algorithm):
        # Store the objectives of the previous generation
        generation = algorithm.n_gen
        self.generation_count = generation
        print('gen',f"Generation: {generation}")
        self.PrevPop = algorithm.pop.get("F")

        sol = algorithm.pop[algorithm.pop.get("rank") == 0]
        unique_opt_values = set(tuple(ind.F) for ind in sol)
        unique_opt_list = list(unique_opt_values)
        self.opt = np.array(unique_opt_list)
        print('opt',self.opt)
        # Access the normalized population from the selection class
        # norm_pop = MyBinaryTournamentSelection.NormPop
        # if norm_pop is not None:
        #     print('norm pop in callback', norm_pop.get("F"))
        # if MyBinaryTournamentSelection.selection_count == 1:
        #     self.PrevPop=MyBinaryTournamentSelection.NormPop.get("F")
        #     algorithm.pop.set("F",self.PrevPop)
        #     print('opt',algorithm.pop.get("F"))

    def notify_new_fitness(self, new_fitness_values,algorithm):
        # Update the objectives of the previous generation with new fitness values
        self.PrevPop = new_fitness_values.get("F")  # Assuming new_fitness_values is a numpy array
        algorithm.pop.set("F", self.PrevPop)

        print('VALUE HEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE',self.PrevPop)
        print('VALUE HEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEALGO', algorithm.pop.get("F"))
        print('called')
