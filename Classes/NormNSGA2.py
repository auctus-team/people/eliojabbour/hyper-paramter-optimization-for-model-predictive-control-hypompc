
import numpy as np
from pymoo.operators.crossover.ux import UniformCrossover
from pymoo.operators.crossover.sbx import SBX
from pymoo.core.problem import ElementwiseProblem
from pymoo.operators.crossover.ux import UniformCrossover
from pymoo.util.misc import random_permuations
from pymoo.problems import get_problem

from Classes.MyEvaluator import MyEvaluator
from Classes.MyCallback import MyCallback
from Classes.MySelection import MyBinaryTournamentSelection

from Classes.MPC_1Dd import MPCSimulator
from Classes.MPC_1Dd import GenerateStep
import numpy as np
import math
import warnings
from pymoo.operators.crossover.ux import UniformCrossover
from pymoo.operators.crossover.sbx import SBX
from pymoo.core.problem import ElementwiseProblem
from pymoo.operators.crossover.ux import UniformCrossover
from pymoo.util.misc import random_permuations
from pymoo.operators.mutation.bitflip import BitflipMutation
from pymoo.core.individual import Individual
from pymoo.core.population import Population
from pymoo.core.problem import Problem
from pymoo.operators.selection.tournament import TournamentSelection,compare
from pymoo.algorithms.moo.nsga3 import NSGA3

from pymoo.optimize import minimize
from numpy.linalg import LinAlgError
from pymoo.core.evaluator import Evaluator
from pymoo.core.population import Population
from pymoo.visualization.scatter import Scatter
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting
from pymoo.operators.survival.rank_and_crowding import RankAndCrowding
from pymoo.operators.sampling.rnd import FloatRandomSampling
from pymoo.operators.selection.tournament import compare, TournamentSelection
from pymoo.operators.mutation.pm import PolynomialMutation, PM
from pymoo.operators.repair.rounding import RoundingRepair

from pymoo.operators.sampling.lhs import LHS

import matplotlib.pyplot as plt
from pymoo.visualization.pcp import PCP
from pymoo.visualization.heatmap import Heatmap
from pymoo.visualization.petal import Petal
from pymoo.util.dominator import Dominator

from datetime import datetime




def generate_unique_filename(optimization_func_name):
    timestamp = datetime.now().strftime("%m%d%H%")
    filename = f"{optimization_func_name}.npz"
    return filename


class MPCOptimizationProblem(ElementwiseProblem):
    def __init__(self, total_time, dTm, qx,Xcons, Ucons,t, delta_time, generated_x,dx,lower,upper):
        """
        Initializes the MPCOptimizationProblem class.

        Parameters:
        - total_time: Total simulation time.
        - dTm: Time step for the MPC simulation.
        - qx: Weight for state penalty in the MPC optimization.
        - Xcons: State constraints for the simulation.
        - Ucons: Input constraints for the simulation.
        - t: Time vector for the simulation.
        - delta_time: Time step between samples.
        - generated_x: Reference state position for the simulation.
        - dx: Reference velocity
        """
        super().__init__(n_var=3, n_obj=3, n_constr=0, xl=lower, xu=upper)
        self.total_time = total_time
        self.dTm = dTm
        self.qx = qx
        self.Xcons = Xcons
        self.Ucons = Ucons
        self.t = t
        self.delta_time = delta_time
        self.generated_x = generated_x
        self.dx = dx
        self.xl=lower
        self.xu=upper
        self.ideal_point = [100.0,100.0,100.0]
        self.nadir_point=[-1.0,-1.0,-1.0]
        self.extreme_points = None
        

    def _evaluate(self, x, out, *args, **kwargs):
        """
        Evaluates the optimization problem.

        Parameters:
        - x: Decision variable containing MPC parameters (TimeMPC, qdx, gamma).
        - out: Output object to store the objective function values.

        Raises:
        - Exception: If there is an error in the simulation.
        """
        # Unpack MPC parameters from the decision variable 'x'
        TimeMPC, qdx, gamma = x
        TimeMPC = round(TimeMPC / self.dTm) * self.dTm
        # Ngen = MyCallback.generation_count
        # print(Ngen)
        # Define constraints for your simulation
        mpc_simulator = MPCSimulator()

        try:
            # Run the simulation with current MPC parameters
            results = mpc_simulator.run_MPC_simulation(TimeMPC, self.dTm, self.qx, qdx, gamma, self.generated_x,
                                     self.dx,
                                     self.total_time, self.delta_time, self.Xcons, self.Ucons, QTermCost=[1.0, qdx],
                                     RTermCost=[gamma])

            # Extract relevant simulation results
            elapsed_simulation_time = results['Maxmpctime']
            print('elapsed_simulation_time',elapsed_simulation_time)

            if elapsed_simulation_time > 0.01:
                ObjCost = np.array([
                    results['OvershootingRatio'] + 10000.0 * elapsed_simulation_time,
                    results['Final_Error'] + 10000.0 * elapsed_simulation_time,
                    results['rise_time'] + 10000.0 * elapsed_simulation_time
                ])
            else:
                ObjCost = np.array([
                    results['OvershootingRatio'],
                    results['Final_Error'],
                    results['rise_time']
                ])
            print('ObjCost',ObjCost)

            out["F"] = ObjCost

        except Exception as e:
            print(f"Error in simulation: {e}")
            # Penalize the objectives in case of simulation failure
            out["F"] = np.array([1e3, 1e3, 1e3])















# if __name__ == "__main__":
#     # Define the parameters for your optimization
#     total_time = 10.0
#     max_overshoot = 5.0
#     max_error = 0.01
#     max_rise_time = 1.0
#     max_elapsed_sim_time = 0.01
#     dTm = 0.01
#     qx = 1.0
#     # Example usage
#     threshold_time = 2.0
#     total_time = 10.0
#     delta_time = 0.01
#     start_time = 0.0
#     step_value = 1.0

#     # We want to test our MPC on a step function with a threshold time of 2 seconds and a total time of 10 seconds with a step value of 0.5:
#     t, x,dx= GenerateStep(threshold_time, total_time, delta_time, start_time,step_value)
#     delta_time = 0.01

#     # Create an instance of the objective MPC problem
#     mpc_problem = MPCOptimizationProblem(total_time, max_overshoot, max_error, max_rise_time, dTm, qx, t, delta_time,
#                                           x)
#     selection = MyBinaryTournamentSelection()
#     callback = MyCallback()
#     # evaluator=MyEvaluator(skip_already_evaluated=False)
#     mutation=PM(eta=20)
#     # Define the NSGA3 algorithm
#     algorithm = NSGA2(pop_size=1000, sampling=LHS(),
#                       eliminate_duplicates=True,selection=selection,crossover =UniformCrossover(),evaluator=MyEvaluator(skip_already_evaluated=False))

#     # Create an instance of the callback class and set it to the algorithm

#     # Run NSGA3 optimization and saving the history for future analysis in another file
#     result = minimize(problem=mpc_problem, algorithm=algorithm, seed=1, termination=('n_gen', 100), save_history=True, verbose=True,
#                       callback=callback)

#     # Visualize the results
#     Scatter().add(result.F).show()
#     Scatter().add(result.F[:, [2, 0]]).show()
#     PCP().add(result.F).show()
#     Heatmap().add(result.F).show()
#     print(result)
#     # Generate a unique filename based on the optimization function name and bounds
#     filename = generate_unique_filename("History")

#     # Save optimization results to a file
#     np.savez('TuesdNightNadiroptNormalized2000gen50popUnconsNga2.npz', X=result.X, F=result.F, G=result.G, CV=result.CV,
#              algorithm=result.algorithm,
#              opt=result.opt, pop=result.pop, history=result.history)

#     plot = Scatter()
#     plot.add(mpc_problem.pareto_front(), plot_type="line", color="black", alpha=0.7)
#     plot.add(result.F, color="red")
#     plot.show()

#     val = [e.opt.get("F")[0] for e in result.history]
#     plt.plot(np.arange(len(val)), val)
#     plt.show()
# pass

