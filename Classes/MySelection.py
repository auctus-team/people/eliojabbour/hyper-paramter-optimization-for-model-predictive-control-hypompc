from pymoo.core.selection import Selection
from pymoo.operators.crossover.ux import UniformCrossover
from pymoo.core.problem import ElementwiseProblem, Problem


from pymoo.operators.mutation.bitflip import BitflipMutation
from pymoo.core.individual import Individual
from pymoo.core.population import Population
from pymoo.operators.selection.tournament import TournamentSelection, compare
from pymoo.algorithms.moo.nsga3 import NSGA3
from pymoo.optimize import minimize
from numpy.linalg import LinAlgError
from pymoo.core.evaluator import Evaluator
from pymoo.visualization.scatter import Scatter
from pymoo.problems import get_problem
from pymoo.algorithms.moo.nsga2 import NSGA2
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting
from pymoo.operators.survival.rank_and_crowding import RankAndCrowding
from pymoo.operators.sampling.rnd import FloatRandomSampling
from pymoo.operators.mutation.pm import PolynomialMutation, PM
from pymoo.operators.repair.rounding import RoundingRepair
from pymoo.util.misc import random_permuations
import math
import numpy as np

from pymoo.util.dominator import Dominator

class MyBinaryTournamentSelection(Selection):
    """
    This class represents a binary tournament selection strategy for evolutionary algorithms.

    Attributes:
        tournament_type (str): The type of tournament selection to be performed.
        pressure (float): The pressure parameter for the tournament selection.
        ideal_point (list): The ideal point for the optimization problem.
        nadir_point (list): The nadir point for the optimization problem.
    """
    def __init__(self) -> None:
        super().__init__()
        self.tournament_type = 'comp_by_rank_and_crowding'
        self.pressure=4.0
        self.ideal_point=[100,100,100]
        self.nadir_point=[0,0,0]

    def _do(self, problem, pop, n_select, n_parents, **kwargs):
        """
        Perform binary tournament selection on the given population.

        Args:
            problem: The optimization problem.
            pop: The population to perform selection on.
            n_select (int): The number of individuals to select.
            n_parents (int): The number of parents to select for each individual.

        Returns:
            numpy.ndarray: The selected individuals.

        Raises:
            SomeException: An exception that may occur.
        """

        # print('Current Pop',pop.get("F"))
        n_pop = len(pop) // 2

        _, rank = NonDominatedSorting().do(pop.get('F'), return_rank=True)
        # print('rank:', rank)

        Pc = (rank[:n_pop] == 0).sum() / len(pop)
        Pd = (rank[n_pop:] == 0).sum() / len(pop)

        # print('Pc:', Pc)
        # print('Pd:', Pd)

        # number of random individuals needed
        n_random = n_select * n_parents * self.pressure
        # print('n_random before conversion:', n_random)

        # Convert n_random to an integer
        n_random = int(n_random)
        # print('n_random after conversion:', n_random)

        n_perms = math.ceil(n_random / n_pop)
        # print('n_perms:', n_perms)

        # get random permutations and reshape them
        P = random_permuations(n_perms, n_pop)[:n_random]
        # print('P:', P)

        P = np.reshape(P, (n_select * n_parents, int(self.pressure)))
        # print('Reshaped P:', P)

        if Pc <= Pd:
            # Choose from DA
            P[::n_parents, :] += n_pop

        pf = np.random.random(n_select)
        P[1::n_parents, :][pf >= Pc] += n_pop

        # print('Final P after adjustment:', P)

        # compare using tournament function
        S = self.comp_by_cv_dom_then_random(pop, P)

        # print('Selected parents:', S)

        return np.reshape(S, (n_select, n_parents))


    def binary_tournament(self, a, rank_a, cd_a, b, rank_b, cd_b):
        if rank_a < rank_b:
            return a
        elif rank_a > rank_b:
            return b
        else:
            if cd_a < cd_b:
                return a
            elif cd_a > cd_b:
                return b
            else:
                return np.random.choice([a, b])
            
    def comp_by_cv_dom_then_random(self,pop, P):
        S = np.full(P.shape[0], np.nan)

        for i in range(P.shape[0]):
            a, b = P[i, 0], P[i, 1]

            if pop[a].CV <= 0.0 and pop[b].CV <= 0.0:
                rel = Dominator.get_relation(pop[a].F, pop[b].F)
                if rel == 1:
                    S[i] = a
                elif rel == -1:
                    S[i] = b
                else:
                    S[i] = np.random.choice([a, b])
            elif pop[a].CV <= 0.0:
                S[i] = a
            elif pop[b].CV <= 0.0:
                S[i] = b
            else:
                S[i] = np.random.choice([a, b])

        return S[:, None].astype(int)