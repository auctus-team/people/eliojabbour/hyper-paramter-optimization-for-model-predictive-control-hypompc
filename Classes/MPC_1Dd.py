import numpy as np
from qpsolvers import solve_qp
from scipy import sparse
import time
import matplotlib.pyplot as plt

class MPCSimulator:

    def modify_last_diagonals(self, Q_diag, new_values):
        """
        Modify the last N diagonal elements of a square matrix based on a vector of new values.

        Parameters:
        - Q_diag: Square matrix
        - new_values: Vector of new values to replace the last N diagonal elements

        Returns:
        - Modified matrix
        """

        Q_modified = Q_diag.copy()
        Q_modified[-len(new_values):, -len(new_values):] = np.diag(new_values)
        return Q_modified

    def generate_B_hat(self, A, B, N):
        """
        Generate the block matrix B_hat for a given system (A, B) and prediction horizon N.

        Parameters:
        - A: State matrix
        - B: Input matrix
        - N: Prediction horizon

        Returns:
        - Block matrix B_hat
        """
        m, n = B.shape
        A_shape = A.shape
        B_hat = np.empty((N * m, N * n))

        for i in range(N):
            for j in range(N):
                if j <= i:
                    Aj = np.eye(*A_shape)
                    for k in range(i - j):
                        Aj = np.dot(Aj, A)
                    B_block = np.dot(Aj, B)
                    B_hat[i * m:(i + 1) * m, j * n:(j + 1) * n] = B_block
                else:
                    B_block = np.zeros((m, n))
                    B_hat[i * m:(i + 1) * m, j * n:(j + 1) * n] = B_block

        return B_hat

    def generate_C_hat(self, C, N):
        """
        Generate the block matrix C_hat

        Parameters:
        - C: Input matrix
        - N: Prediction horizon

        Returns:
        - Block matrix C_hat
        """
        m, n = C.shape
        C_hat = np.zeros((N * m, N * n))

        for i in range(N):
            C_hat[i * m:(i + 1) * m, i * n:(i + 1) * n] = C

        return C_hat

    def generate_A_hat(self, A, N):
        """
        Generate the block matrix A_hat for a given state matrix A and prediction horizon N.

        Parameters:
        - A: State matrix
        - N: Prediction horizon

        Returns:
        - Block matrix A_hat
        """
        r, c = A.shape
        A_hat = np.zeros((N * r, c))

        Ak = A
        for k in range(N):
            A_hat[k * r: (k + 1) * r, :] = Ak
            Ak = np.dot(A, Ak)

        return A_hat

    def run_MPC_simulation(self,TimeMPC, dTm, qx, qdx, gamma, x, dx, total_time, delta_time, Xcons, Ucons,QTermCost,RTermCost):
        """
        Run a Model Predictive Control (MPC) simulation for a 1D system.

        Parameters:
        - TimeMPC: Total time for the MPC simulation
        - dTm: Time step for the MPC controller
        - qx: Weighting factor for position in the cost function
        - qdx: Weighting factor for velocity in the cost function
        - gamma: Weighting factor for control input in the cost function
        - x: Reference position of the system
        - dx: Reference velocity of the system
        - total_time: Total time duration of the simulation
        - delta_time: Time step between samples
        - Xcons: Constraints on the state [xmin, xmax, dxmin, dxmax]
        - Ucons: Constraints on the control input [umin, umax]
        - QTermCost: Weighting matrix for terminal cost in the position and velocity
        - RTermCost: Weighting matrix for terminal control input  in the cost function

        Returns:
        - Results of the MPC simulation
        """
        # Define state space matrices
        start_time = time.time()  # Record the start time of the simulation
        N = int(TimeMPC / dTm)  # Calculate the number of steps based on TimeMPC

        # Define matrix A as a 2x2 NumPy array
        A = np.array([[1, dTm],
                      [0, 1]])

        # Define matrix B as a 2x1 NumPy array
        B = np.array([[dTm ** 2 / 2],
                      [dTm]])

        # Generate A_hat and B_hat using functions
        A_hat = self.generate_A_hat(A, N)
        B_hat = self.generate_B_hat(A, B, N)

        # Initialize state vectors and inputs
        # X0 = np.array([x[0],dx[0]])
        X0 = np.array([0.0, 0.0])
        Uk = np.zeros(N * len(B[0]))
        Xk = np.dot(A_hat, X0) + np.dot(B_hat, Uk)

        # Extract state constraints from Xcons
        xmin = Xcons[0]  # Minimum position constraint
        xmax = Xcons[1]  # Maximum position constraint
        dxmin = Xcons[2]  # Minimum velocity constraint
        dxmax = Xcons[3]  # Maximum velocity constraint

            # Define constraint matrices for state (position and velocity)
        Cx = np.array([[-1, 0],
                       [1, 0],
                       [0, -1],
                       [0, 1]])

        # Extract constraints on control input acceleration from Ucons
        ddxmin = Ucons[0]  # Minimum control input acceleration
        ddxmax = Ucons[1]  # Maximum control input acceleration

        # Define equality constraints for control input
        Fueq = np.array([-ddxmin, ddxmax])

        # Define constraint matrices for control input
        Cu = np.array([[-1],
                       [1]])

        # Define equality constraints for state
        Fxeq = np.array([-xmin, xmax, -dxmin, dxmax])

        # Create an array of repeated equality constraints for each time step
        Fxeq_hat = [Fxeq] * N
        Fxeq_hat = [item for sublist in Fxeq_hat for item in sublist]
        Fxeq_hat = np.array(Fxeq_hat)

        # Generate augmented matrices for control input constraints
        Cx_hat = self.generate_C_hat(Cx, N)

        # Calculate matrices Ax_ineq and Bx_ineq for inequality constraints
        Ax_ineq = np.dot(Cx_hat, B_hat)
        Bx_ineq = Fxeq_hat - np.dot(Cx_hat, np.dot(A_hat, X0))


        # Create propagated equality constraints for control input on all the horizon
        Fueq_hat = [Fueq] * N
        Fueq_hat = [item for sublist in Fueq_hat for item in sublist]
        Fueq_hat = np.array(Fueq_hat)

        # Calculate matrices Bu_ineq and Au_ineq
        Bu_ineq = Fueq_hat
        Cu_hat = self.generate_C_hat(Cu, N)
        Au_ineq = Cu_hat

        # Combine propagated state and control input inequality constraints
        A_ineq = np.vstack((Ax_ineq, Au_ineq))
        B_ineq = np.concatenate((Bx_ineq, Bu_ineq))

        # Create the reference state matrix for the entire MPC horizon as initialization
        Xref = np.tile([[x[0]], [dx[0]]], (N, 1))

           # Create the diagonal matrix with repeated qx and qdx for all the horizon length
        Q_diag = np.diagflat([qx, qdx] * (N))
        Q_diag = self.modify_last_diagonals(Q_diag, QTermCost)
        # Create the control input cost for all the horizon
        R_diag = np.diag([gamma] * N)
        R_diag = self.modify_last_diagonals(R_diag, RTermCost)

        # Calculate the Hessian matrix H for the quadratic cost function
        H = 2 * np.dot(B_hat.T, np.dot(Q_diag, B_hat)) + 2 * R_diag
        H = sparse.triu(H).tocsc()

        # Precompute constants outside the loop
        B_hat_Q_diag = np.dot(B_hat.T, Q_diag)
        B_hat_Q_diag_A_hat = np.dot(B_hat_Q_diag, A_hat)


        # Initialize variables for MPC simulation and analysis
        X0_storage = []  # List to store initial states
        time1 = 0.0  # Reset time for each iteration
        index = 0  # Reset index for each iteration
        xn, dxn, ddxn = [], [], []  # Lists to store system states of the MPC
        storedPreviousX = np.zeros((150, 1))  # Initialize an array to store previous states
        storePredictedU = []  # List to store predicted control inputs
        time_vector, timme_vectorPrev = [], []  # Lists to store time values
        rise_start_time, rise_end_time = None, None  # Variables to track rise time
        threshold_10_percent = 0.1 * x[-1]  # Threshold for 10% of final value
        threshold_90_percent = 0.9 * x[-1]  # Threshold for 90% of final value
        rise_time, Error, ErrorSum, Ierr, ROverShooting, Maxelapsed_time = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0  # Initialize analysis variables


        while time1 < total_time - 0.001:
            start_time = time.time()
            # Solve the QP problem
            # Create the initial state vector X0 for the MPC
            X0 = np.array([Xk[0], Xk[1]])
            # Create the reference state vector Xref for the MPC
            Xref = np.tile([[x[index]], [dx[index]]], (N, 1))
            # Calculate the terms for the optimal control input U
            f1 = 2 * np.dot(B_hat_Q_diag_A_hat, X0)  # Term related to the state matrix A_hat
            f2 = -2 * np.dot(B_hat_Q_diag, Xref)  # Term related to the reference state vector Xref
            f = np.add(f1.reshape(-1, 1), f2.reshape(-1, 1))  # Combine the two terms
            # Calculate the inequality constraints on the state (Bx_ineq) and control input (Bu_ineq) which is updated in each timestep
            Bx_ineq = Fxeq_hat - np.dot(Cx_hat, np.dot(A_hat, X0))
            B_ineq = np.concatenate((Bx_ineq, Bu_ineq))
            # Solve the Quadratic Programming (QP) problem to obtain the optimal control input U
            U = solve_qp(H, f, A_ineq, B_ineq, None, None, solver="osqp", max_iter=10000, verbose=False)
            # Update the state vector Xk
            Xk = np.dot(A_hat, X0) + np.dot(B_hat, U)
            # Append the current position Xk[0] to the list of system state positions
            xn.append(Xk[0])
            # Append the current velocity Xk[1] to the list of velocities
            dxn.append(Xk[1])

            ddxn.append(U[0])
            # if index == 150:
            #     plotindex = index
            #     storedPreviousX = ddxn  # Append previous X values
            #     storePredictedU.extend([U])  # Append predicted control input
            #     timme_vectorPrev.extend([time_vector])
            # # Check if xn is near 10% threshold to calculate the rise time

            if rise_start_time is None and Xk[0] > threshold_10_percent:
                rise_start_time = time1
            if rise_end_time is None and Xk[0] > threshold_90_percent:
                rise_end_time = time1
                rise_time = rise_end_time - rise_start_time

            # Error = abs(x[index] - Xk[0])
            # ErrorSum = ErrorSum + Error

            time_vector.append(time1)  # Append the current time to the list
            time1 += delta_time
            index += 1
            elapsed_time = time.time() - start_time
            Maxelapsed_time=max(Maxelapsed_time,elapsed_time) # Calculate the maximum amount of time the QP took to solve a problem set.




        # Calculate the final error between the desired and actual final positions
        FinalError = abs(x[-1] - xn[-1])

        # Check if the rise_end_time is still None, indicating no rise time occurred
        if rise_end_time is None:
            rise_time = total_time  # Set rise_time to the total simulation time

        # Calculate the percentage overshooting based on the maximum position reached
        ROverShooting = 100 * (max(xn) - x[-1]) / x[-1]
        # Ensure ROverShooting is non-negative, set to 0 if calculated as negative(undershoot)
        if ROverShooting < 0.0:
            ROverShooting = 0.0

        return {
                'xn': xn,  # List of all the first ith calculated MPC state positions used in the next time step
                'dxn': dxn,  # List of first ith velocities used in the MPC
                'ddxn': ddxn,  # List of the first control inputs for the horizon across simulation timesteps
                'time_vector': time_vector,  # List of time values corresponding to each simulation timestep
                'elapsed_time': elapsed_time,  # Time taken to complete the MPC simulation loop
                'rise_time': rise_time,  # Time taken for the system state to rise from 10% to 90% of the final value
                'Final_Error': FinalError,  # Absolute difference between the final desired position and the last calculated position
                'OvershootingRatio': ROverShooting,  # Percentage of overshooting relative to the final desired position
                'Maxmpctime': Maxelapsed_time  # Maximum time taken by the Quadratic Programming solver for a single problem set
            }



def GenerateStep(threshold, total_time, delta_time, start_time, Step_Height):
    """
    Generate a step function with specified characteristics.

    Parameters:
    - threshold: Time at which the step function transitions
    - total_time: Total time duration of the step function
    - delta_time: Time step between samples
    - start_time: Starting time of the step function
    - Step_Height: The position goal of the step.

    Returns:
    - time: Array of time values
    - step_function: Generated step function
    """
        # Number of steps
    num_steps = int(total_time / delta_time)

    # Create an array of time values
    time = np.linspace(start_time, start_time + total_time, num_steps + 1)

    # Compute the step function using the Heaviside function
    step_function = Step_Height* np.heaviside(time - threshold, 1)

    # Compute the velocity by taking the derivative of the step function
    velocity = 0 * np.diff(step_function) / delta_time

    # Compute the acceleration by taking the derivative of the velocity
    acceleration = 0 * np.diff(velocity) / delta_time

    return time, step_function,velocity